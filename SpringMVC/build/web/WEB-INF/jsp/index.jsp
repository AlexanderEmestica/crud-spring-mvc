<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Spring Read</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            html, body{
                margin: 0px;
                padding: 0px;
            }
        </style>
    </head>

    <body>
        <div class="container-fluid">
            <div class="card border-info">
                <div class="card-header bg-info text-white">
                    <a href="agregar.htm" class="btn btn-light">Agregar Nuevo Registro</a>
                </div>
                <div class="card-body">
                    <table class="table table-hover" style="text-align: center;">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Nombres</td>
                                <td>Apellidos</td>
                                <td>DUI</td>
                                <td>Telefono</td>
                                <td>Genero</td>
                                <td>
                                    Acciones
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="datos" items="${lista}">
                                <tr>
                                    <td>${datos.id_persona}</td>
                                    <td>
                                        ${datos.primer_nombre}
                                        ${datos.segundo_nombre}
                                    </td>
                                    <td>
                                        ${datos.primer_apellido}
                                        ${datos.segundo_apellido}
                                    </td>
                                    <td>${datos.dui}</td>
                                    <td>${datos.telefono}</td>
                                    <td>${datos.genero}</td>
                                    <td>
                                        <a href="editar.htm?id=${datos.id_persona}" class="btn btn-warning">Editar</a>
                                        <a href="eliminar.htm?id=${datos.id_persona}" class="btn btn-danger">Eliminar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>