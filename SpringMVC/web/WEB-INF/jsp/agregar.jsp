<%-- 
    Document   : agregar
    Created on : 09-01-2019, 02:02:09 PM
    Author     : Alex Eméstica
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agregar</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <div class="container-fluid">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h1>Agregar Nuevo Registro</h1>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <label>Primer Nombre: </label>
                        <input type="text" name="primer_nombre" class="form-control">
                        <label>Segundo Nombre: </label>
                        <input type="text" name="segundo_nombre" class="form-control">
                        <label>Primer Apellido: </label>
                        <input type="text" name="primer_apellido" class="form-control">
                        <label>Segundo Apellido: </label>
                        <input type="text" name="segundo_apellido" class="form-control">
                        <label>DUI: </label>
                        <input type="text" name="dui" class="form-control">
                        <label>Telefono: </label>
                        <input type="text" name="telefono" class="form-control">
                        <label>Genero: </label>
                        <input type="text" name="genero" class="form-control">
                        <input type="submit" value="Agregar" class="btn btn-primary">
                        <a href="index.htm">Regresar</a>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
