<%-- 
    Document   : Editar
    Created on : 09-02-2019, 08:36:17 AM
    Author     : alexander.emesticaus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <div class="container-fluid">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h1>Actualizar Registro</h1>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <label>Primer Nombre: </label>
                        <input type="text" name="primer_nombre" class="form-control" value="${lista[0].primer_nombre}">
                        <label>Segundo Nombre: </label>
                        <input type="text" name="segundo_nombre" class="form-control" value="${lista[0].segundo_nombre}">
                        <label>Primer Apellido: </label>
                        <input type="text" name="primer_apellido" class="form-control" value="${lista[0].primer_apellido}">
                        <label>Segundo Apellido: </label>
                        <input type="text" name="segundo_apellido" class="form-control" value="${lista[0].segundo_apellido}">
                        <label>DUI: </label>
                        <input type="text" name="dui" class="form-control" value="${lista[0].dui}">
                        <label>Telefono: </label>
                        <input type="text" name="telefono" class="form-control" value="${lista[0].telefono}">
                        <label>Genero: </label>
                        <input type="text" name="genero" class="form-control" value="${lista[0].genero}">
                        <input type="submit" value="Actualizar" class="btn btn-primary">
                        <a href="index.htm">Regresar</a>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
