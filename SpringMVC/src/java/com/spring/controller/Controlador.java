package com.spring.controller;

import com.spring.conexion.Conexion;
import com.spring.entities.Snj_clientes;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Alex Eméstica
 */
@Controller
public class Controlador {

    Conexion connect = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(connect.conectar());
    ModelAndView mav = new ModelAndView();
    int id;
    List listaClientes;

    @RequestMapping("index.htm")
    public ModelAndView listar() {
        String sql = "select * from snj_clientes";
        listaClientes = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista", listaClientes);
        mav.setViewName("index");
        return mav;
    }

    @RequestMapping(value = "agregar.htm", method = RequestMethod.GET)
    public ModelAndView agregar() {
        mav.addObject(new Snj_clientes());
        mav.setViewName("agregar");
        return mav;
    }

    @RequestMapping(value = "agregar.htm", method = RequestMethod.POST)
    public ModelAndView agregar(Snj_clientes sc) {
        String sql = "insert into snj_clientes (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, dui, telefono, genero) values(?,?,?,?,?,?,?)";
        this.jdbcTemplate.update(sql, sc.getPrimer_nombre(), sc.getSegundo_nombre(), sc.getPrimer_apellido(), sc.getSegundo_apellido(), sc.getDui(), sc.getTelefono(), sc.getGenero());
        return new ModelAndView("redirect:/index.htm");
    }
    
    @RequestMapping(value = "editar.htm", method = RequestMethod.GET)
    public ModelAndView Editar(HttpServletRequest request){
        id=Integer.parseInt(request.getParameter("id"));
        String sql = "select * from snj_clientes where id_persona="+id;
        listaClientes = jdbcTemplate.queryForList(sql);
        mav.addObject("lista", listaClientes);
        mav.setViewName("editar");
        return mav;
    }
    
    @RequestMapping(value = "editar.htm", method = RequestMethod.POST)
    public ModelAndView Editar(Snj_clientes sc){
        String sql = "update snj_clientes set primer_nombre=?, segundo_nombre=?, primer_apellido=?, segundo_apellido=?, dui=?, telefono=?, genero=? where id_persona="+id;
        this.jdbcTemplate.update(sql, sc.getPrimer_nombre(), sc.getSegundo_nombre(), sc.getPrimer_apellido(), sc.getSegundo_apellido(), sc.getDui(), sc.getTelefono(), sc.getGenero());
        return new ModelAndView("redirect:/index.htm");
    }
    
    @RequestMapping("eliminar.htm")
    public ModelAndView Eliminar(HttpServletRequest request){
        id=Integer.parseInt(request.getParameter("id"));
        String sql = "delete from snj_clientes where id_persona="+id;
        this.jdbcTemplate.update(sql);
        return new ModelAndView("redirect:/index.htm");
    }

}
