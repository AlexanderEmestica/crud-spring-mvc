package com.spring.conexion;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author Alex Eméstica
 */
public class Conexion {

    public DriverManagerDataSource conectar() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/springnetbeansjasper");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        return dataSource;
    }
}
